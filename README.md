# HelloCloud

---

A simple microservice that's built on cloud0 toolkit.

## Get started

Run with docker-compose 

```
docker-compose up -d
```

After starting success, the app exposes to port 8088. For the first time, 
you need to call migration & seeding test data:

```
curl -X POST http://localhost:8088/internal/migrate
curl -X POST http://localhost:8088/internal/seed
```

Then you could try some functions

- list object meta

```shell
curl 'http://localhost:8088/object-meta?page_size=10&order=id'
```

- create object meta

```shell
curl -X POST -H 'x-user-id: 23ed5d7a-b7a6-4de7-b4ed-7bc50efa89cc' 'http://localhost:8088/object-meta' \
  -d '{
          "object_id" : "40195220-2421-4044-8b0f-41c1fa019b03",
          "object_type_key" : "test",
          "meta_key" : "test",
          "meta_value" : "test",
          "meta_json_value" : {"1": 222}
      }'
```

- update object meta

```shell
curl -X PUT -H 'x-user-id: 23ed5d7a-b7a6-4de7-b4ed-7bc50efa89cc' 'http://localhost:8088/object-meta/23ed5d7a-b7a6-4de7-b4ed-7bc50efa89cc' \
  -d '{
          "object_id" : "40195220-2421-4044-8b0f-41c1fa019b03",
          "object_type_key" : "test",
          "meta_key" : "test",
          "meta_value" : "test",
          "meta_json_value" : {"1": 222}
      }'
```
