module gitlab.com/jxhq/ecom/ms-term

go 1.16

//replace gitlab.com/goxp/cloud0 v1.0.0 => /ws/goxp/cloud0

require (
	github.com/astaxie/beego v1.12.3 // indirect
	github.com/caarlos0/env/v6 v6.6.2
	github.com/gin-gonic/gin v1.7.2
	github.com/google/uuid v1.2.0
	github.com/jinzhu/gorm v1.9.16
	github.com/praslar/lib v0.1.2
	github.com/sendgrid/rest v2.6.4+incompatible // indirect
	gitlab.com/goxp/cloud0 v1.1.1
	gorm.io/gorm v1.21.11
	gorm.io/plugin/soft_delete v1.0.2
)
