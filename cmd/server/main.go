package main

import (
	"context"
	"gitlab.com/jxhq/ecom/ms-term/conf"
	"os"
	"gitlab.com/goxp/cloud0/logger"
	"gitlab.com/jxhq/ecom/ms-term/pkg/service"
)

func main() {
	logger.Init()
	conf.SetEnv()
	_ = os.Setenv("PORT", conf.LoadEnv().Port)
	_ = os.Setenv("DB_HOST", conf.LoadEnv().DBHost)
	_ = os.Setenv("DB_PORT", conf.LoadEnv().DBPort)
	_ = os.Setenv("DB_USER", conf.LoadEnv().DBUser)
	_ = os.Setenv("DB_PASS", conf.LoadEnv().DBPass)
	_ = os.Setenv("DB_NAME", conf.LoadEnv().DBName)
	_ = os.Setenv("ENABLE_DB", conf.LoadEnv().EnableDB)
	app := service.NewService()
	ctx := context.Background()
	err := app.Start(ctx)
	if err != nil {
		logger.Tag("main").Error(err)
	}
	os.Clearenv()
}
