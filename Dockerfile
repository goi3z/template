FROM golang:1.16 as builder

WORKDIR /code
ADD go.mod .
ADD go.sum .

RUN go mod download

ADD . .
RUN go build -o server ./cmd/server

FROM ubuntu:20.04
WORKDIR /code
COPY --from=builder /code/server .
ADD resources ./resources

CMD "/code/server"
