package handlers

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/jxhq/ecom/ms-term/pkg/model"
	"gorm.io/gorm"
)

type MigrationHandler struct {
	db *gorm.DB
}

func NewMigrationHandler(db *gorm.DB) *MigrationHandler {
	return &MigrationHandler{db: db}
}


func (h *MigrationHandler) Migrate(ctx *gin.Context) {
	_ = h.db.Exec("CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\"")

	models := []interface{}{
		&model.ObjectMeta{},
	}
	for _, m := range models {
		err := h.db.AutoMigrate(m)
		if err != nil {
			_ = ctx.Error(err)
			return
		}
	}
}

// Seed seeds some records in DB
//func (h *MigrationHandler) Seed(ctx *gin.Context) {
//	bytes, err := ioutil.ReadFile("resources/items.json")
//	if err != nil {
//		_ = ctx.Error(err)
//		return
//	}
//
//	var data []*repo.Item
//	err = json.Unmarshal(bytes, &data)
//	if err != nil {
//		_ = ctx.Error(err)
//		return
//	}
//
//	for _, item := range data {
//		if item.State == 0 {
//			item.State = repo.StateNew
//		}
//	}
//
//	err = h.db.Create(data).Error
//	if err != nil {
//		_ = ctx.Error(err)
//		return
//	}
//
//	ctx.Status(http.StatusNoContent)
//}
