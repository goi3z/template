package handlers

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/praslar/lib/common"
	"gitlab.com/goxp/cloud0/ginext"
	"gitlab.com/goxp/cloud0/logger"
	"gitlab.com/jxhq/ecom/ms-term/pkg/model"
	"gitlab.com/jxhq/ecom/ms-term/pkg/repo"
	"gitlab.com/jxhq/ecom/ms-term/pkg/utils"
	"net/http"
)

type ObjectMetaHandlers struct {
	objectMeta *repo.ObjectMetaRepo
}

func NewObjectMetaHandlers(repo *repo.ObjectMetaRepo) *ObjectMetaHandlers {
	return &ObjectMetaHandlers{objectMeta: repo}
}

type ObjectMetaListRequest struct {
	ObjectID      uuid.UUID
	ObjectTypeKey string
	MetaKey       string
	MetaValue     string
}

func (h *ObjectMetaHandlers) Create(c *gin.Context) {
	ctx := ginext.WrapGinContext(c)
	log := logger.WithCtx(ctx, "ItemHandler.Create")

	owner, err := ginext.GetUUIDUserID(c)
	if err != nil {
		_ = c.Error(err)
		return
	}

	req := model.ObjectMetaRequest{}
	if err := c.ShouldBindJSON(&req); err != nil {
		_ = c.Error(err)
		return
	}
	if err := common.CheckRequireValid(req); err != nil {
		_ = c.Error(err)
		return
	}

	objectMeta := &model.ObjectMeta{
		BaseModel: model.BaseModel{
			CreatorID: &owner,
		},
	}
	common.Sync(req, objectMeta)

	err = h.objectMeta.Create(objectMeta)
	if err != nil {
		log.WithError(err).WithField("request", req).Error("failed to create object meta")
		_ = c.Error(err)
		return
	}
	c.JSON(http.StatusCreated, objectMeta)
}


func (h *ObjectMetaHandlers) List(c *gin.Context) {
	l := logger.WithCtx(c, "ObjectMetaHandlers.List")

	req := ObjectMetaListRequest{}
	err := c.ShouldBindQuery(&req)
	if err != nil {
		_ = c.Error(err)
		return
	}

	filter := &model.ObjectMetaFilter{
		ObjectID:      req.ObjectID,
		ObjectTypeKey: req.ObjectTypeKey,
		MetaKey:       req.MetaKey,
		MetaValue:     req.MetaValue,
		Pager:         ginext.NewPagerWithGinCtx(c, l),
	}

	result, err := h.objectMeta.Filter(c, filter)
	if err != nil {
		l.WithError(err).WithField("filter", filter).Error("failed to query filter object meta")
		_ = c.Error(err)
		return
	}

	resp := ginext.NewPaginatedResponse(result.Records, result.Filter.Pager)
	c.JSON(http.StatusOK, resp)
}

func (h *ObjectMetaHandlers) GetOne(c *gin.Context) {
	ctx := ginext.WrapGinContext(c)
	l := logger.WithCtx(ctx, "ObjectMetaHandlers.GetOne")

	ID := &uuid.UUID{}
	if  ID = utils.ParseIDFromUri(c); ID == nil {
		_ = c.Error(fmt.Errorf("Error: empty id from uri"))
		return
	}

	objectMeta, err := h.objectMeta.Get(*ID)
	if err != nil {
		l.WithError(err).WithField("objectMeta.id", *ID).Error("failed to query objectMeta")
		_ = c.Error(ginext.NewError(http.StatusInternalServerError, "Server error"))
		return
	}
	c.JSON(http.StatusOK, objectMeta)
}

func (h *ObjectMetaHandlers) Update(c *gin.Context) {
	ctx := ginext.WrapGinContext(c)
	l := logger.WithCtx(ctx, "ObjectMetaHandlers.Update")

	currentUser, err := ginext.GetUUIDUserID(c)
	if err != nil {
		_ = c.Error(err)
		return
	}

	req := model.ObjectMetaRequest{}
	if  req.ID = utils.ParseIDFromUri(c); req.ID == nil {
		_ = c.Error(fmt.Errorf("Error: empty id from uri"))
		return
	}

	if err := c.ShouldBindJSON(&req); err != nil {
		_ = c.Error(err)
		return
	}

	objectMeta, err := h.objectMeta.Get(*req.ID)
	if err != nil {
		l.WithError(err).WithField("objectMeta.id", req.ID).Error("failed to query objectMeta")
		_ = c.Error(ginext.NewError(http.StatusInternalServerError, "Server error"))
		return
	}
	// TODO: require currentUser to update if need
	//if objectMeta.CreatorID != currentUser {
	//	_ = c.Error(ginext.NewError(http.StatusForbidden, "unauthorized to modify this objectMeta"))
	//	return
	//}
	objectMeta.UpdaterID = &currentUser
	common.Sync(req, objectMeta)
	if err = h.objectMeta.Update(ctx, objectMeta); err != nil {
		_ = c.Error(err)
		return
	}
	c.JSON(http.StatusOK, objectMeta)
}

func (h *ObjectMetaHandlers) Delete(c *gin.Context) {
	ctx := ginext.WrapGinContext(c)
	l := logger.WithCtx(ctx, "ObjectMetaHandlers.Delete")

	currentUser, err := ginext.GetUUIDUserID(c)
	if err != nil {
		_ = c.Error(err)
		return
	}

	req := model.ObjectMetaRequest{}
	if  req.ID = utils.ParseIDFromUri(c); req.ID == nil {
		_ = c.Error(fmt.Errorf("Error: empty id from uri"))
		return
	}

	objectMeta, err := h.objectMeta.Get(*req.ID)
	if err != nil {
		l.WithError(err).WithField("objectMeta.id", req.ID).Error("failed to query objectMeta")
		_ = c.Error(ginext.NewError(http.StatusNotFound, "Record Not Found"))
		return
	}
	// TODO: require owner to update if need
	if objectMeta.CreatorID != &currentUser {
		_ = c.Error(ginext.NewError(http.StatusForbidden, "unauthorized to modify this objectMeta"))
		return
	}
	objectMeta.UpdaterID = &currentUser
	if err = h.objectMeta.Delete(ctx, objectMeta); err != nil {
		_ = c.Error(err)
		return
	}

	c.Status(http.StatusOK)
}
