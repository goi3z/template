package service

import (
	"gitlab.com/goxp/cloud0/ginext"
	"gitlab.com/goxp/cloud0/service"
	"gitlab.com/jxhq/ecom/ms-term/pkg/handlers"
	"gitlab.com/jxhq/ecom/ms-term/pkg/repo"
)

type extraSetting struct {
	DbDebugEnable bool `env:"DB_DEBUG_ENABLE" envDefault:"false"`
}

type Service struct {
	*service.BaseApp
	setting *extraSetting
}

func NewService() *Service {
	s := &Service{
		service.NewApp("ms-term", "v1.0"),
		&extraSetting{},
	}
	objectMetaRepo := repo.NewObjectMetaRepo(s.GetDB(), s.setting.DbDebugEnable)
	objectMetaHandle := handlers.NewObjectMetaHandlers(objectMetaRepo)

	// Object Meta
	s.Router.POST("/object-meta",  ginext.AuthRequiredMiddleware, objectMetaHandle.Create)
	s.Router.GET("/object-meta/:id", objectMetaHandle.GetOne)
	s.Router.PUT("/object-meta/:id",  ginext.AuthRequiredMiddleware, objectMetaHandle.Update)
	s.Router.DELETE("/object-meta/:id",ginext.AuthRequiredMiddleware, objectMetaHandle.Delete)
	s.Router.GET("/object-meta", ginext.AuthRequiredMiddleware, objectMetaHandle.List)

	// Migrate
	migrateHandler := handlers.NewMigrationHandler(s.GetDB())
	s.Router.POST("/internal/migrate", migrateHandler.Migrate)
	//s.Router.POST("/internal/seed", migrateHandler.Seed)

	return s
}
