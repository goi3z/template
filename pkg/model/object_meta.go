package model

import (
	"github.com/google/uuid"
	"github.com/jinzhu/gorm/dialects/postgres"
	"gitlab.com/goxp/cloud0/ginext"
)

type ObjectMeta struct {
	BaseModel
	ObjectID      uuid.UUID      `gorm:"not null;index:idx_object_meta_object_id" json:"object_id"`                   //TODO: Set FK
	ObjectTypeKey string         `gorm:"not null;type:varchar(255);index:idx_object_type_key" json:"object_type_key"` // TODO: Set FK
	MetaKey       string         `gorm:"index:idx_object_meta_meta_key;type:varchar(255)" json:"meta_key"`
	MetaValue     string         `json:"meta_value"`
	MetaJsonValue postgres.Jsonb `gorm:"type:jsonb" json:"meta_json_value"`
}

type ObjectMetaRequest struct {
	ID            *uuid.UUID      `json:"id,omitempty"`
	UpdaterID     *uuid.UUID      `json:"updater_id,omitempty"`
	CreatorID     *uuid.UUID      `json:"creator_id,omitempty"`
	ObjectID      *uuid.UUID      `json:"object_id,omitempty" valid:"Required"`
	ObjectTypeKey *string         `json:"object_type_key,omitempty"`
	MetaKey       *string         `json:"meta_key,omitempty" valid:"Required"`
	MetaValue     *string         `json:"meta_value,omitempty"`
	MetaJsonValue *postgres.Jsonb `json:"meta_json_value,omitempty"`
}

type ObjectMetaFilter struct {
	ObjectID      uuid.UUID
	ObjectTypeKey string
	MetaKey       string
	MetaValue     string
	Pager         *ginext.Pager
}

type ObjectMetaFilterResult struct {
	Filter  *ObjectMetaFilter
	Records []*ObjectMeta
}

func (r *ObjectMeta) TableName() string {
	return "object_meta"
}
