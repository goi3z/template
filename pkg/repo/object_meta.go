package repo

import (
	"context"
	"github.com/google/uuid"
	"gitlab.com/goxp/cloud0/logger"
	"gitlab.com/jxhq/ecom/ms-term/pkg/model"
	"gorm.io/gorm"
)

type ObjectMetaRepo struct {
	db *gorm.DB
	debug bool
}

func NewObjectMetaRepo(db *gorm.DB, debug bool) *ObjectMetaRepo {
	if debug {
		logger.Tag("global.NewObjectMetaRepo").Warn("DB_DEBUG is enabled on ObjectMetaRepo, please use it with care")
	}

	return &ObjectMetaRepo{db: db, debug: debug}
}

func (r *ObjectMetaRepo) Create(objectMeta *model.ObjectMeta) error {
	return r.db.Create(objectMeta).Error
}

func (r *ObjectMetaRepo) Get(id uuid.UUID) (*model.ObjectMeta, error) {
	o := &model.ObjectMeta{}
	err := r.db.First(&o, id).Error
	return o, err
}

func (r *ObjectMetaRepo) Filter(ctx context.Context, f *model.ObjectMetaFilter) (*model.ObjectMetaFilterResult, error) {
	tx := r.db.WithContext(ctx).Model(&model.ObjectMeta{})
	log := logger.WithCtx(ctx, "ObjectMetaRepo.Filter")

	if f.MetaKey != "" {
		tx = tx.Where("meta_key iLIKE ?", f.MetaKey)
	}
	if f.MetaValue != "" {
		tx = tx.Where("meta_value = ?", f.MetaValue)
	}
	if f.ObjectID != uuid.Nil {
		tx = tx.Where("object_id = ?", f.ObjectID)
	}
	if f.ObjectTypeKey != "" {
		tx = tx.Where("object_type_key = ?", f.ObjectTypeKey)
	}

	result := &model.ObjectMetaFilterResult{
		Filter:  f,
		Records: []*model.ObjectMeta{},
	}

	pager := result.Filter.Pager

	tx = pager.DoQuery(&result.Records, tx)
	if tx.Error != nil {
		log.WithError(tx.Error).Error("error while filter items")
	}

	return result, tx.Error
}

func (r *ObjectMetaRepo) Update(ctx context.Context, update *model.ObjectMeta) error {
	return r.db.WithContext(ctx).Where("id = ?", update.ID).Save(&update).Error
}

func (r *ObjectMetaRepo) Delete(ctx context.Context, d *model.ObjectMeta) error {
	return r.db.WithContext(ctx).Delete(&d).Error
}