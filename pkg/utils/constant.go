package utils

const (
	HeaderXRequestID = "x-request-id"
	HeaderUserID     = "x-user-id"
	HeaderUserMeta   = "x-user-type"
)
